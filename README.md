### Helpful commands
1. Generate a new app - `rails new project_name`
2. Create a new model - `rails generate model model_name`
3. Start the rails server `rails server`


### Install Rails
**you will need nodejs, yarn, and a sqlite instance**
