# Number Guessing Game

## Description

Create a number guessing game in which the computer picks a number and you have to guess it.  The computer will give you cues like 'Too High' or 'Too Low.'


## Instructions

Your goal is to creae a number guessing game. Your program should pick a random number between 1 and 100 and ask you for a guess. If your guess is less than the program's number, it should tell you that you were low and let you guess again. If the guess is greater than the program's number, it should tell you that you were high and let you guess again. If your guess is correct, the program should tell you that you win and then quit.

Other requirements:

* After 5 incorrect guesses, the program should tell you that you lose.
* If you guess the same number twice, the program should ask you if you're feeling all right (or something similarly sarcastic).
* Your code should include at least two methods.
* This game should be run from the command line by typing "ruby guessing_game.rb".
* (optional)The program could also comment on your behavior if you make a guess that doesn't help you.  For example, you might say "50" and then be told "that's too low."  If you then guess "25," you're just wasting a guess.